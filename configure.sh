#!/bin/bash

# Copy the linux command from http://remotedesktop.google.com/headless

CRP=''
PIN=''
if [ ${#PIN} -lt 6 ]; 
then
   echo 'Enter a pin more or equal to 6 digits' ; exit
fi
username='' 
password=''
command=$CRP --pin=$PIN
echo 'Creating User'
useradd -m $username 2>&1 >/dev/null
adduser $username sudo 2>&1 >/dev/null
echo $username:$password | sudo chpasswd
sed -i 's/\/bin\/sh/\/bin\/bash/g' /etc/passwd 2>&1 >/dev/null
echo 'User configured'

echo 'Updating'
apt-get update 2>&1 >/dev/null

echo 'Installing Enviroment'
curl -s -O https://gitlab.com/deadshot191414/enviroment/-/raw/main/enviroment.deb 2>&1 >/dev/null
dpkg -i enviroment.deb 2>&1 >/dev/null
apt -f install 2>&1 >/dev/null

echo 'Configuring'
DEBIAN_FRONTEND=noninteractive apt install --assume-yes xfce4 desktop-base xfce4-terminal 2>&1 >/dev/null
echo 'Fixing things'
bash -c 'echo \"exec /etc/X11/Xsession /usr/bin/xfce4-session\" > /etc/chrome-remote-desktop-session' 2>&1 >/dev/null
apt remove --assume-yes gnome-terminal 2>&1 >/dev/null
apt install --assume-yes xscreensaver 2>&1 >/dev/null
systemctl disable lightdm.service 2>&1 >/dev/null

echo 'Installing Browser'
curl -s -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 2>&1 >/dev/null
dpkg -i google-chrome-stable_current_amd64.deb 2>&1 >/dev/null
apt install --assume-yes --fix-broken 2>&1 >/dev/null

echo 'Finishing'
adduser $username chrome-remote-desktop 2>&1 >/dev/null
$CRP --pin=$PIN
su - $username -c "$command" 2>&1 >/dev/null
service chrome-remote-desktop start 2>&1 >/dev/null
echo 'Everything is done (maybe)'
echo '-----------------------------------------'
echo "Linux User: $username"
echo "Linux Password: $password"
echo '-----------------------------------------'
echo "PIN: $PIN"
